package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.*;

// ref: https://gitlab.com/racendol/advprog-tutorial/
@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    @Test
    public void testProduceKnight(){
        academyService = new AcademyServiceImpl(new AcademyRepository());
        assertThat(academyService.getKnightAcademies().size()).isNotEqualTo(0);

        academyService.produceKnight("Lordran", "majestic");

        assertTrue(academyService.getKnight() instanceof MajesticKnight);
    }


    @Test
    public void whenGetKnightAcademiesIsCalledItShouldCallGetKnightAcademies(){
        academyService.getKnightAcademies();
        verify(academyRepository, times(1)).getKnightAcademies();
    }
}
