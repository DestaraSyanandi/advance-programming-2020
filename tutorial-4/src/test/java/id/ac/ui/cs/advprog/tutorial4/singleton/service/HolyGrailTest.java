package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    HolyGrail holyGrail;
    @BeforeEach
    public void setUp() throws Exception{
        holyGrail = new HolyGrail();
        holyGrail.makeAWish("I wanna graduate from pusilkom");
    }

    @Test
    public void testSetWish(){
        holyGrail.makeAWish("I wanna graduate from FT");
        assertEquals("I wanna graduate from FT", holyGrail.getHolyWish().getWish());
    }

    @Test
    public void testGetWish(){

        assertEquals("I wanna graduate from pusilkom", holyGrail.getHolyWish().getWish());
    }
    // TODO create tests
}
