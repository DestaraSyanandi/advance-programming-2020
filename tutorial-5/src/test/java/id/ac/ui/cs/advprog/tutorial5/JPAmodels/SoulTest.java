package id.ac.ui.cs.advprog.tutorial5.JPAmodels;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new SoulBuilder().setName("Nama").setAge(12).setGender("M").setOccupation("ASD").createSoul();
        soul.setId(1);
    }

    @Test
    void getGender() {
        assertEquals("M",soul.getGender());
    }

    @Test
    void setGender() {
        soul.setGender("L");
        assertEquals("L",soul.getGender());
    }

    @Test
    void getOccupation() {
        assertEquals("ASD",soul.getOccupation());
    }

    @Test
    void setOccupation() {
        soul.setOccupation("asdasd");
        assertEquals("asdasd",soul.getOccupation());
    }

    @Test
    void getName() {
        assertEquals("Nama",soul.getName());
    }

    @Test
    void setName() {
        soul.setName("1");
        assertEquals("1",soul.getName());
    }

    @Test
    void getAge() {
        assertEquals(12,soul.getAge());
    }

    @Test
    void setAge() {
        soul.setAge(1);
        assertEquals(1,soul.getAge());
    }

    @Test
    void getId() {
        assertEquals(1,soul.getId());
    }

    @Test
    void setId() {
        soul.setId(2);
        assertEquals(2,soul.getId());
    }
}
