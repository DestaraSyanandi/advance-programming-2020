package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.JPAmodels.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Signature;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/soul")
public class SoulController {

    @Autowired
    private SoulService soulService;

    @GetMapping
    public ResponseEntity<List<Soul>> findAll(){
        return new ResponseEntity<>(soulService.findAll(),HttpStatus.OK);
//        return null;
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Soul soul){
        soulService.register(soul);
        return new ResponseEntity(HttpStatus.OK);
//        return null;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id){
        Optional<Soul> optionalSoul = soulService.findSoul(id);
        if(!optionalSoul.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Soul>(optionalSoul.get(),HttpStatus.OK);
//        return null;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul){
        Optional<Soul> optionalSoul = soulService.findSoul(id);
        if(!optionalSoul.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        soul.setId(id);
        return new ResponseEntity<>(soulService.rewrite(soul),HttpStatus.OK);
//        return null;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        Optional<Soul> optionalSoul = soulService.findSoul(id);
        if(!optionalSoul.isPresent()){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        soulService.erase(id);
        return new ResponseEntity(HttpStatus.OK);
    }


}
