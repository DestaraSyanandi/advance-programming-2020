package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals(member.getName(), "Wati");
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(), "Gold Merchant");
        //TODO: Complete me
    }

    @Test
    public void testMethodAddChildMember() {
        Member panjdi = new OrdinaryMember("Panjdi", "Petualang");
        member.addChildMember(panjdi);

        List<Member> members = member.getChildMembers();
        assertEquals(members.size(), 1);
        assertEquals(members.get(0).getName(), "Panjdi");

        //TODO: Complete me
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member panjdi = new OrdinaryMember("Panjdi", "Petualang");
        member.addChildMember(panjdi);
        member.removeChildMember(panjdi);

        List<Member> members = member.getChildMembers();
        assertEquals(members.size(), 0);

        //TODO: Complete me
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        for(int i =0;i<5;i++) {
            Member anggota = new OrdinaryMember("Anggota "+i, "partyMember");
            member.addChildMember(anggota);
        }

        List<Member> members = member.getChildMembers();
        assertEquals(members.size(), 3);
        assertEquals(members.get(0).getName(), "Anggota 0");
        assertEquals(members.get(1).getName(), "Anggota 1");
        assertEquals(members.get(2).getName(), "Anggota 2");

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member pandji = new PremiumMember("Panjdi", "Master");

        for(int i =0;i<5;i++) {
            Member anggota = new OrdinaryMember("Anggota "+i, "partyMember");
            pandji.addChildMember(anggota);
        }
        List<Member> members = pandji.getChildMembers();
        assertEquals(members.get(0).getName(), "Anggota 0");
        assertEquals(members.get(1).getName(), "Anggota 1");
        assertEquals(members.get(2).getName(), "Anggota 2");
        assertEquals(members.get(3).getName(), "Anggota 3");
        assertEquals(members.get(4).getName(), "Anggota 4");

    }
}
