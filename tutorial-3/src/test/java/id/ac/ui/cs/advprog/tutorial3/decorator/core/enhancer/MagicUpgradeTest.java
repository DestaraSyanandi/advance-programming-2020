package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Longbow", magicUpgrade.getName());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals("Magic Big Longbow", magicUpgrade.getDescription());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(30 <= magicUpgrade.getWeaponValue());
        assertTrue(magicUpgrade.getWeaponValue() <= 35);
        //TODO: Complete me
    }
}
