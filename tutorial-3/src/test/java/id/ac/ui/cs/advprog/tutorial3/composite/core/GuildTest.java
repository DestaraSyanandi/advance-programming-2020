package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        Member congil = new PremiumMember("Congil", "Istrigafi");
        guild.addMember(guildMaster, congil);

        Member tatan = new PremiumMember("Tatan", "Istrigafi");
        guild.addMember(congil, tatan);

        Member lina = new OrdinaryMember("Lina", "Istrigafi");
        guild.addMember(tatan, lina);

        Member found = guild.getMember("Lina", "Istrigafi");
        assertNotNull(found);
        assertEquals(found.getName(), "Lina");

        guild.removeMember(congil, tatan);

        found = guild.getMember("Lina", "Istrigafi");
        assertNull(found);

        found = guild.getMember("Tatan", "Istrigafi");
        assertNull(found);

        //TODO: Complete me
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member reiza = new OrdinaryMember("nafla", "Servant");
        guild.addMember(guildMaster, reiza);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(reiza  , guild.getMemberHierarchy().get(0).getChildMembers().get(0));

    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member orang = new PremiumMember("premium3", "role3");
        guild.addMember(guildMaster, orang);
        Member getMember = guild.getMember("premium3", "role3");

        assertEquals(getMember, orang);

    }
}
