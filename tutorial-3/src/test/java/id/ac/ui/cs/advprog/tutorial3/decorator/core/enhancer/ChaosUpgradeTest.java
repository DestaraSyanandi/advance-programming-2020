package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Gun", chaosUpgrade.getName());
        //TODO: Complete me
    }

    @Test
    public void testGetMethodWeaponDescription(){
        assertEquals("Chaos Automatic Gun", chaosUpgrade.getDescription());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(70 <= chaosUpgrade.getWeaponValue() );
        assertTrue(chaosUpgrade.getWeaponValue() <= 75);

        //TODO: Complete me
    }

}
