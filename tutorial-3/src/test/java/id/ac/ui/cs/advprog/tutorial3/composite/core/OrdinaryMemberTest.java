package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals(member.getName(), "Nina");
        //TODO: Complete me
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(), "Merchant");
        //TODO: Complete me
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member izaki = new OrdinaryMember("Izaki", "Beater");
        member.addChildMember(izaki);
        assertEquals(member.getChildMembers().size(), 0);


        member.removeChildMember(izaki);
        assertEquals(member.getChildMembers().size(), 0);

    }
}
