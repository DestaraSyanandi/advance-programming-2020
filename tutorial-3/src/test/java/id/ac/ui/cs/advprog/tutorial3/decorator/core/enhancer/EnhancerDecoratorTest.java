package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;



public class EnhancerDecoratorTest {

    Weapon weapon1 = new Gun();
    Weapon weapon2 = new Gun();
    Weapon weapon3 = new Gun();
    Weapon weapon4 = new Gun();
    Weapon weapon5 = new Gun();

    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        //TODO: Complete me
        assertEquals("Chaos Automatic Gun", weapon1.getDescription());
        assertEquals("Magic Automatic Gun", weapon2.getDescription());
        assertEquals("Regular Automatic Gun", weapon3.getDescription());
        assertEquals("Raw Automatic Gun", weapon4.getDescription());
        assertEquals("Unique Automatic Gun", weapon5.getDescription());

    }

}
