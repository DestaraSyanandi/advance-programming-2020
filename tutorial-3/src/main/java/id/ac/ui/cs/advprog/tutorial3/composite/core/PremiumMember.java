package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private String name, roleplay;
    private List<Member> memberList;
    public PremiumMember(String name, String role) {
        this.name = name;
        this.roleplay = role;
        memberList = new ArrayList<>();


    }
    @Override
    public String getRole() {
        return roleplay;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void addChildMember(Member member) {

        if(memberList.size() < 3 || roleplay.compareTo("Master") == 0) {
            memberList.add(member);
        }
    }
    @Override
    public void removeChildMember(Member member) {
        memberList.remove(member);
    }
    @Override
    public List<Member> getChildMembers() {
        return memberList;
    }

    //TODO: Complete me
}
