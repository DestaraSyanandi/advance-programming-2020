package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    RNG enchancerng;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
        enchancerng = new RNG(15,20);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        return enchancerng.generate() + weapon.getWeaponValue();
        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "Magic "+weapon.getDescription();
        //TODO: Complete me
    }
}
