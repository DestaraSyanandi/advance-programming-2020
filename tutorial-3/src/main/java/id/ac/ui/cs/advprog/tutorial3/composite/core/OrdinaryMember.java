package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    private String name, roleplay;

    public OrdinaryMember(String name, String roleplay) {
        this.name = name;
        this.roleplay = roleplay;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return roleplay;
    }

    @Override
    public void addChildMember(Member member) {

    }

    @Override
    public void removeChildMember(Member member) {

    }

    @Override
    public List<Member> getChildMembers() {
        return new ArrayList<>();
    }



    //TODO: Complete me
}
