package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    RNG enchancerng;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
        enchancerng = new RNG(10, 15);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete
        return enchancerng.generate() + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Unique "+weapon.getDescription();
    }
}
