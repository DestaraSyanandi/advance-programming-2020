package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    RNG enchancerng;
    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
        enchancerng = new RNG(50,55);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return enchancerng.generate() + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return "Chaos "+weapon.getDescription();
        //TODO: Complete me
    }
}
