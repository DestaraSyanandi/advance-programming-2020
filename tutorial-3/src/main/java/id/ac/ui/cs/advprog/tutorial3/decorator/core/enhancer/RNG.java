package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;
import java.lang.Math;
public class RNG {

    private int min, max;
    public RNG(int min, int max) {
        this.min = min;
        this.max = max;
    }

    int generate() {
        int x = (int)(Math.random()*((max-min)+1))+min;
        return x;
    }
}

