package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild ;
                //ToDo: Complete Me
        }
        @Override
        public void update(){
                String type = this.guild.getQuestType();
                if (type.equalsIgnoreCase("R")||type.equalsIgnoreCase("D"))
                        this.getQuests().add(this.guild.getQuest());
        }

        //ToDo: Complete Me
}
