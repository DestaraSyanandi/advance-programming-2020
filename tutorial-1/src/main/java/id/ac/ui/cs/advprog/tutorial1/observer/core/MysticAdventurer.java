package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic"; this.guild = guild;

                //ToDo: Complete Me
        }

        public void update() {
                String type = this.guild.getQuestType();
                if (type.equalsIgnoreCase("E") || type.equalsIgnoreCase("D") )
                        this.getQuests().add(this.guild.getQuest());
        }

        //ToDo: Complete Me
}
